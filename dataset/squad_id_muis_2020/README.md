# SQuAD ID

Reorganized SQuAD ID dataset.

Paper Title: Sequence-to-sequence learning for indonesian automatic question generator

Venue: ICAICTA 2020

## References

- https://github.com/IndoNLP/nusa-crowd/blob/master/nusacrowd/nusa_datasets/squad_id/squad_id.py
- https://ieeexplore.ieee.org/document/9429032
- https://indonlp.github.io/nusa-catalogue/card.html?squad_id
- https://github.com/FerdiantJoshua/question-generator
